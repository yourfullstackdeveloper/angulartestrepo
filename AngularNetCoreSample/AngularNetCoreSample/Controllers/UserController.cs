﻿using AngularNetCoreSample.DAL;
using AngularNetCoreSample.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularNetCoreSample.Controllers
{
    public class UserController : Controller
    {
        Repository repository = new Repository();
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("api/testuser/index")]
        public IEnumerable<TestUser> GetTestUsers()
        {
            var list = repository.GetTestUsers();
            return list;
        }
    }
}
