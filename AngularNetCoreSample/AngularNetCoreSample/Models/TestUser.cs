﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AngularNetCoreSample.Models
{
    public partial class TestUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public bool? Status { get; set; }
    }
}
