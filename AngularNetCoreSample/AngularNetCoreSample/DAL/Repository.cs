﻿using AngularNetCoreSample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularNetCoreSample.DAL
{
    public class Repository
    {
        testDBContext context = new testDBContext();

        public IEnumerable<TestUser> GetTestUsers()
        {
            try
            {
                var list = context.TestUsers.ToList();
                return list;
            }
            catch
            {
                return null;
            }
        }
    }
}
