import { Component, OnInit } from '@angular/core';
import { TestUserModel, TestuserService } from '../services/testuser.service';

@Component({
  selector: 'app-test-user',
  templateUrl: './test-user.component.html',
  styleUrls: ['./test-user.component.css']
})
export class TestUserComponent implements OnInit {

  public testusers: TestUserModel[];

  constructor(private testuserService: TestuserService) {
    this.getAllTestUsers();
  }

  ngOnInit() {
  }

  getAllTestUsers() {
    
    this.testuserService.getAllTestUsers().subscribe(data => {
      this.testusers = data;
      console.log(this.testusers);
    })
  }

}
