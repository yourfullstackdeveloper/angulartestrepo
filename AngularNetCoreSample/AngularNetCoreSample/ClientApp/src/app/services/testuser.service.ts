import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestuserService {

  constructor(private http: HttpClient) { }

  getAllTestUsers(): Observable<TestUserModel[]> {
    var data = this.http.get<TestUserModel[]>("api/testuser/index");
    return data;
  }
}

export class TestUserModel {
  id: number;
  name: string;
  address: string;
  status: boolean;
}
